class Persona:
    def __init__(self, n, a, f, d):
        self._nombre = n
        self._apellidos = a
        self._fecha_nacimiento = f 
        self._dni = d
    
    def __str__(self):
        return f'Nombre: {self._nombre}, Apellidos: {self._apellidos}, Fecha de nacimiento: {self._fecha_nacimiento}, DNI: {self._dni}'


    def setNombre(self, n):
        self._nombre = n
    def getNombre(self):
        return self._nombre 
    def setApellidos(self, a):
        self._apellidos = a
    def getApellidos(self):
        return self._apellidos 
    def setFecha_nacimiento(self, n):
        self._fecha_nacimiento = f
    def getFecha_nacimiento(self):
        return self._fecha_nacimiento
    def setDni(self, d):
        self._dni = d
    def getDni(self):
        return self._dni


class Paciente(Persona):
    def __init__(self, n, a, f, d, h):
        super().__init__(n, a, f, d)
        self._historial_clinico = h
    def ver_historial_clinico(self):
        return self._historial_clinico


class Medico(Persona):
    def __init__(self, n, a, f, d, e, c):
        super().__init__(n, a, f, d)   
        self._especialidad = e
        self._citas = c
    def consultar_agenda(self):
        return f'Especialidad: {self._especialidad}, Citas: {self._citas}'

if __name__ == "__main__": #esto hace que en el test no se ejecute
    persona01 = Persona("María", "Magdalena Galleta", "01/04/1970", "15202301H")
    persona01.setNombre("José")

    persona02 = Paciente("María", "Magdalena Galleta", "01/04/1970", "15202301H", "fractura")
    print(persona02.ver_historial_clinico())

    persona03 = Medico("María", "Magdalena Galleta", "01/04/1970", "15202301H", "trauma", "12 febrero")
    print(persona03.consultar_agenda())
#TENEMOS QUE HACER TEST DE LOS MÉTODOS DE MÉDICO Y PACIENTE