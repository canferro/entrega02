import unittest

from persona import Paciente
from persona import Medico

class TestPaciente(unittest.TestCase):
    def test_ver_historial_clinico01(self):
        persona01 = Paciente("María", "Magdalena Galleta", "01/04/1970", "15202301H", "fractura")
        self.assertEqual(persona01._historial_clinico, "fractura")
   
class TestMedico(unittest.TestCase):
    def test_consultar_agenda01(self):
        persona02 = Medico("María", "Magdalena Galleta", "01/04/1970", "15202301H", "Traumatología", "12 febrero")
        agenda = persona02.consultar_agenda()
        self.assertEqual(agenda, 'Especialidad: Traumatología, Citas: 12 febrero')





unittest.main()